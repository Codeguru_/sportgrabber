﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;
using System.Web.Http.Results;
using DapperMagician.Core.Core.Interfaces;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using SportGrabber.Api.Controllers;
using SportGrabber.Core.Enums;
using SportGrabber.Core.Interfaces;
using SportGrabber.Core.Models;

namespace SportGrabber.Api.Tests
{
    [TestFixture]
    public class GamesControllerTests
    {
        private readonly Mock<IDapperMagicianRepository<Game>> _repository;
        private readonly Mock<IRepositoryFactory> _repositoryFactoryMock;
        private readonly GamesController _target;
        private readonly Fixture _fixture;
        private readonly List<Game> _testData;

        public GamesControllerTests()
        {
            _fixture = new Fixture();
            _repositoryFactoryMock = new Mock<IRepositoryFactory>();
            _repository = new Mock<IDapperMagicianRepository<Game>>();
            _repositoryFactoryMock.Setup(x => x.Create<Game>()).Returns(_repository.Object);
            _target = new GamesController(_repositoryFactoryMock.Object);
            _testData = Enumerable.Range(0, 10).Select(x => new Game
            {
                Id = _fixture.Create<long>(),
                CompetitionName = _fixture.Create<string>(),
                SportType = _fixture.Create<SportType>(),
                Competitor1 = _fixture.Create<string>(),
                Competitor2 = _fixture.Create<string>(),
                Start = _fixture.Create<DateTime?>(),
                End = _fixture.Create<DateTime?>()
            }).ToList();
        }

        [Test]
        public void Get_WhenParameterNull_ReturnsOkWithAllGames()
        {
            var expectedResult = _testData.ToList();
            _repository.Setup(x => x.Values).Returns(expectedResult);

            var result = _target.Get(null) as OkNegotiatedContentResult<IEnumerable<Game>>;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Content, Is.EquivalentTo(expectedResult));
        }

        [Test]
        public void Get_WhenParameterIsValid_ReturnsOkWithFilteredResults()
        {
            var param = _fixture.Create<SportType>();
            var filteredResults = _testData.ToList();
            _repository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Game, bool>>>())).Returns(filteredResults);

            var result = _target.Get(param.ToString()) as OkNegotiatedContentResult<IEnumerable<Game>>;

            _repository.Verify(x => x.FindAll(It.IsAny<Expression<Func<Game, bool>>>()), Times.Once);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Content, Is.EquivalentTo(filteredResults));
        }

        [Test]
        public void Get_WhenParameterIsValid_WithInvalidParameter_ReturnsBadRequest()
        {
            var param = _fixture.Create<string>();

            IHttpActionResult result = _target.Get(param) ;

            Assert.That(result, Is.TypeOf<BadRequestErrorMessageResult>());
        }
    }
}
