﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DapperMagician.Core.CommonTypes;
using SportGrabber.Core.Enums;

namespace SportGrabber.Core.Models
{
    public class Game : Entity<long>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override long Id { get; set; }

        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public SportType SportType { get; set; }
        public string CompetitionName { get; set; }
        public string Competitor1 { get; set; }
        public string Competitor2 { get; set; }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(Game game1, Game game2)
        {
            if (ReferenceEquals(game1, game2))
            {
                return true;
            }
            if (ReferenceEquals(game1, null) ||
                ReferenceEquals(game2, null))
            {
                return false;
            }

            if (!game1.Equals(null) && !game2.Equals(null))
            {
                return
                    game1.SportType == game2.SportType
                    && game1.Competitor1 == game2.Competitor1
                    && game1.Competitor2 == game2.Competitor2
                    && game1.CompetitionName == game2.CompetitionName
                    && game1.Start == game2.Start;
            }
            return game1.Equals(null) && game2.Equals(null);
        }

        public static bool operator !=(Game game1, Game game2)
        {
            return !(game1 == game2);
        }

        public override bool Equals(object other)
        {
            return this == (other as Game);
        }

        public bool Equals(Game other)
        {
            return this == other;
        }
    }
}
