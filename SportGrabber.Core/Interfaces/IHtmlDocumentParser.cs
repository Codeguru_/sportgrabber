﻿using System.Collections.Generic;

namespace SportGrabber.Core.Interfaces
{
    public interface IHtmlDocumentParser<TEntity> where TEntity : class
    {
        IList<TEntity> Parse(string url);
    }
}
