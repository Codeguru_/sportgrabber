﻿using System;
using DapperMagician.Core.CommonTypes;
using DapperMagician.Core.Core.Interfaces;

namespace SportGrabber.Core.Interfaces
{
    public interface IRepositoryFactory
    {
        IDapperMagicianRepository<TEntity> Create<TEntity>() where TEntity : class, IEntity, new();
    }
}
