﻿using System.Collections.Generic;

namespace SportGrabber.Core.Interfaces
{
    public abstract class AbstractDocumentParser<TEntity> : IHtmlDocumentParser<TEntity> where TEntity : class
    {
        protected IHtmlDocumentParsingStrategy<TEntity> ParcingStrategy;

        protected AbstractDocumentParser(IHtmlDocumentParsingStrategy<TEntity> parcingStrategy)
        {
            this.ParcingStrategy = parcingStrategy;
        }

        IList<TEntity> IHtmlDocumentParser<TEntity>.Parse(string url)
        {            
            return ParcingStrategy.Parse(url);
        }
    }
}
