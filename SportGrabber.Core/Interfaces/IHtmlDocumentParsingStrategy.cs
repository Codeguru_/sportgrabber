﻿using System;
using System.Collections.Generic;
using DapperMagician.Core.CommonTypes;

namespace SportGrabber.Core.Interfaces
{
    public interface IHtmlDocumentParsingStrategy<TEntity> where TEntity : class
    {
        IList<TEntity> Parse(string url);
        // Task<IList<TEntity>> ParseAsync(string url);
    }
}
