﻿using System;

namespace SportGrabber.Core.Interfaces
{
    public interface IParsingService
    {
        int UpdateDatabase(string sourceUrl);
    }
}
