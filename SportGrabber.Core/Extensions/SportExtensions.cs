﻿using System;
using SportGrabber.Core.Enums;

namespace SportGrabber.Core.Extensions
{
    public static class SportExtensions
    {
        public static TimeSpan GetGameLength(this SportType sportType)
        {
            switch (sportType)
            {
                case SportType.Basketball:
                {
                    return TimeSpan.FromMinutes(48);
                }
                case SportType.Hockey:
                {
                    return TimeSpan.FromMinutes(60);
                }
                case SportType.Soccer:
                {
                    return TimeSpan.FromMinutes(90);
                }
                default:
                {
                    return TimeSpan.Zero;
                }
            }
        }
    }
}
