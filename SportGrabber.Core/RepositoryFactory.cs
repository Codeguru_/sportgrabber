﻿using System;
using DapperMagician.Core.Core.Interfaces;
using DapperMagician.Core.Repositories;
using SportGrabber.Core.Interfaces;

namespace SportGrabber.Core
{
    public class RepositoryFactory : IRepositoryFactory
    {
        private readonly string _connectionStringName;
        private readonly string _connectionString;

        public RepositoryFactory() : this(null, null)
        {
        }

        public RepositoryFactory(string connectionStringName, string connectionString)
        {
            _connectionStringName = connectionStringName;
            _connectionString = connectionString;
        }

        IDapperMagicianRepository<TEntity> IRepositoryFactory.Create<TEntity>()
        {
            return new DefaultRepository<TEntity>(_connectionStringName, _connectionString);
        }
    }
}
