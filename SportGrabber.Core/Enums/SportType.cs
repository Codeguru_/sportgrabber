﻿namespace SportGrabber.Core.Enums
{
    public enum SportType
    {
        Basketball = 1,
        Hockey = 2,
        Soccer = 3,
        Tennis = 4,
    }
}
