﻿using System;
using SimpleInjector;
using SportGrabber.Core;
using SportGrabber.Core.Interfaces;

namespace SportGrabber.Data.SimpleInjector
{
    public class Bootstrap
    {
        public static void RegisterDependencies(Container container)
        {
            container.Register<IRepositoryFactory>(() => new RepositoryFactory("SportGrabberTest", null));
        }
    }
}
