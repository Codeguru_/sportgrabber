﻿using System;
using DapperMagician.Core.CommonTypes;
using DapperMagician.Core.Models;
using DapperMagician.EntityFW;
using SportGrabber.Core.Models;

namespace SportGrabber.Data.DapperMagician
{
    public static class DapperMagicianConfig
    {
        private static readonly object Locker = new object();
        private static bool _isInitialized;

        public static void Initialize()
        {
            if (!_isInitialized)
            {
                var modelsAssembly = typeof(Game).Assembly;
                new DapperMagicianDatabase
                {
                    ConnectionStringName = "SportGrabberTest",
                    Schema = new DapperMagicianDbSchema
                    {
                        ModelTypesNamespaces = new[] {"SportGrabber.Core.Models"}
                    }
                }.Initialize();

                _isInitialized = true;
            }
        }
    }
}