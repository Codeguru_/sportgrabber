﻿using System;
using SimpleInjector;
using SportGrabber.Core;
using SportGrabber.Core.Interfaces;
using SportGrabber.Core.Models;
using SportGrabber.Parsing.Livescores;

namespace SportGrabber.Parsing.SimpleInjector
{
    public class Bootstrap
    {
        public static void RegisterDependencies(Container container)
        {
            container.Register<IHtmlDocumentParsingStrategy<Game>>(() => new LivescoresDocumentParsingStrategy(), Lifestyle.Transient);
            container.Register<IParsingService, LiveScoresParsingsService>();            
            container.Register<LivescoresDocumentParser>();
        }
    }
}
