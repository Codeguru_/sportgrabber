﻿using System;
using SportGrabber.Core.Interfaces;
using SportGrabber.Core.Models;

namespace SportGrabber.Parsing.Livescores
{
    public class LivescoresDocumentParser : AbstractDocumentParser<Game>
    {
        public LivescoresDocumentParser(IHtmlDocumentParsingStrategy<Game> parcingStrategy) : base(parcingStrategy)
        {
        }
    }
}
