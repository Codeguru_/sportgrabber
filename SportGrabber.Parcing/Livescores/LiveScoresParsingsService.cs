﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using DapperMagician.Core.Core.Interfaces;
using SportGrabber.Core.Interfaces;
using SportGrabber.Core.Models;

namespace SportGrabber.Parsing.Livescores
{
    public class LiveScoresParsingsService : IParsingService
    {
        private readonly IHtmlDocumentParser<Game> _parser;
        private readonly IRepositoryFactory _repositoryFactory;

        public LiveScoresParsingsService(LivescoresDocumentParser parser, IRepositoryFactory repositoryFactory)
        {
            this._parser = parser;
            this._repositoryFactory = repositoryFactory;
        }

        int IParsingService.UpdateDatabase(string sourceUrl)
        {
            var newData = _parser.Parse(sourceUrl);
            var newValues = new List<Game>(newData.Count);
            var existingValues = new List<Game>(newData.Count);
            using (IDapperMagicianRepository<Game> newRepo = _repositoryFactory.Create<Game>())
            {
                List<Game> repoValues = newRepo.Values.ToList();

                foreach (Game game in newData)
                {
                    if (repoValues != null && repoValues.Any(x => x == game))
                    {
                        existingValues.Add(game);
                    }
                    else
                    {
                        newValues.Add(game);
                    }
                }
                if (newValues.Any())
                {

                    newRepo.BulkAddRange(newValues);
                }
                if (existingValues.Any())
                {
                    newRepo.UpdateRange(existingValues);
                }
            }
            return newValues.Count + existingValues.Count;
        }
    }
}
