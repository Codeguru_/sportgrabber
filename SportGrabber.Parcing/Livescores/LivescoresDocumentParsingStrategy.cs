﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using HtmlAgilityPack;
using SportGrabber.Core.Enums;
using SportGrabber.Core.Extensions;
using SportGrabber.Core.Interfaces;
using SportGrabber.Core.Models;

namespace SportGrabber.Parsing.Livescores
{
    public class LivescoresDocumentParsingStrategy : IHtmlDocumentParsingStrategy<Game>
    {
        IList<Game> IHtmlDocumentParsingStrategy<Game>.Parse(string url)
        {
            var htmlDoc = new HtmlWeb().Load(url);
            var result = new List<Game>();

            var competition = string.Empty;

            var stringSportType = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(url.Split('/').LastOrDefault());

            var sportType = (SportType)Enum.Parse(typeof(SportType), stringSportType, true);

            List<HtmlNode> divNodes =
                htmlDoc.DocumentNode.Descendants("div")
                    .Where(d =>
                        d.Attributes.Any(a =>
                            a.Name == "class" && (a.Value == "left" || a.Value == "right fs11")) ||
                        d.Attributes.Contains("data-eid"))
                    .ToList();

            DateTime? date = null;

            foreach (var divNode in divNodes)
            {
                if (divNode.Attributes.Any(a => a.Name == "class" && a.Value == "left"))
                {
                    if (!string.IsNullOrWhiteSpace(divNode.InnerText))
                    {
                        competition = divNode.InnerText.Trim();
                    }
                    continue;
                }

                if (divNode.Attributes.Any(a => a.Name == "class" && a.Value == "right fs11"))
                {
                    var parsedDate =
                        DateTime.ParseExact(divNode.InnerText, " MMMM dd ", CultureInfo.InvariantCulture);
                    date = new DateTime(DateTime.Now.Year, parsedDate.Month, parsedDate.Day);
                    continue;
                }

                List<string> competitors = divNode.Descendants()
                    .Where(x =>
                        x.Attributes.Any(a => a.Name == "class" && a.Value.Contains("ply")))
                    .Select(x => x.InnerText.Trim()).ToList();

                string startTime = divNode
                    .Descendants()
                    .FirstOrDefault(d =>
                        d.Name == "div"
                        && d.Attributes?.FirstOrDefault(a => a.Name == "class")?.Value.Contains("min") == true)
                    ?.InnerText.Replace("&#x27;", "'").Trim();

                DateTime? gameStarts = null;

                var splittedStartTime = startTime?.Split(':');
                if (splittedStartTime?.Length == 2)
                {
                    var hour = Convert.ToInt32(splittedStartTime[0]);
                    var minute =
                        splittedStartTime[1].Length > 2
                            ? Convert.ToInt32(new string(splittedStartTime[1].Take(2).ToArray()))
                            : Convert.ToInt32(splittedStartTime[1]);

                    gameStarts = date.HasValue
                        ? new DateTime(DateTime.Now.Year, date.Value.Month, date.Value.Day, hour, minute, 0)
                        : gameStarts;
                }

                result.Add(new Game()
                {
                    CompetitionName = competition,
                    SportType = sportType,
                    Competitor1 = competitors[0],
                    Competitor2 = competitors[1],
                    Start = gameStarts,
                    End = gameStarts?.Add(sportType.GetGameLength()),
                });
            }

            return result;
        }
    }
}