﻿using System;
using System.Web.Http;

namespace SportGrabber.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            SimpleInjectorConfig.Configure();
        }
    }
}
