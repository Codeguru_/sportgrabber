﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using SportGrabber.Core.Enums;
using SportGrabber.Core.Interfaces;
using SportGrabber.Core.Models;

namespace SportGrabber.Api.Controllers
{
    public class GamesController : ApiController
    {
        private readonly IRepositoryFactory _repositoryFactory;

        public GamesController(IRepositoryFactory repositoryFactory)
        {
            this._repositoryFactory = repositoryFactory;
        }

        [ResponseType(typeof(IEnumerable<Game>))]
        public IHttpActionResult Get(string sportName = null)
        {
            using (var repository = _repositoryFactory.Create<Game>())
            {
                if (sportName == null)
                {
                    return Ok(repository.Values as IEnumerable<Game>);
                }
                if (!Enum.TryParse(sportName, true, out SportType sportType))
                {
                    return BadRequest($"Invalid paramter value - {sportName}");
                }
                return Ok(repository.FindAll(x => x.SportType == sportType));
            }
        }
    }
}