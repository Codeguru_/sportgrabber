﻿    /--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
   /--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
                       What is DapperMagician ?
 /--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
/--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

DapperMagician (later - DM) is a light-weight ORM framework that wraps and extends the
famous micro-ORM Dapper, allowing to create a fully operative data access layer in
literally a few lines of code. With DM it's possible to start working with database
making CRUD operations as soon as your project has some models and connection string
is specified.

In case it was not, the DM will try to use the first connection string found in a config.

DM can create database for you (with DapperMagician.EntityFW nuget package).
Using of DapperMagician.SqLite module allows us to work with SqLite databases.

    /--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
   /--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
                       How do I start using DM ?
 /--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
/--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

It is very easy to start using DM.

Create a new console application project and inside of it add a new folder 'Models'.
Open NuGeT package console and install DapperMagician and DapperMagician.EntityFW.

Create classes that will represent your models, for instance :

    [Table("Goods")]
    public class Item : Entity<long>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override long Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
    }

Note how we used attributes from System.Data.ComponentModel namespace to provide a
database schema. It'll allow DM to build a customized database schema. Note that you can
use Dapper namespace as well.

Now, in Main method, let's configure DapperMagician to work with SQL server :

	static void Main(string[] args)
	{
	   var database = new DapperMagicianDatabase()
	   {
			ConnectionString="Data Source=MyDataSource;Initial Catalog=MyDatabaseName;Integrated Security=SSPI;",
	   };

	   database.Initialize(DbInitializerMode.DropCreateAlways);

	   AddSomeItems();

	   Console.ReadKey();
	}

Now when we run the program the database is created automatically but before that
let's create method that will add some items to our newly created database:

    public static IEnumerable<Item> AddSomeItems()
    {
        using (var repo = new DefaultRepository<Item>())
        {
            var cupOfCoffee = new Item() { Name = "CupOfCoffee", Price = 3.44 };
            var otherItems = new[]
            {
                new Item() {Name = "Tea", Price = 2.08},
                new Item() {Name = "Sweets", Price = 4.99}
            };

            cupOfCoffee = repo.Add(cupOfCoffee);
            otherItems = repo.AddRange(otherItems).ToArray();

            return new[] {cupOfCoffee}.Concat(otherItems);
        }
    }

It is time to run the program. Our database is created and all values added.
In console window we can see that Id's have been populated.


    /--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
   /--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
                       Questions and Answers
 /--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
/--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

Q: What the hell! The reflection is very SLOW! Why do you use it?

A:
Yes, that is correct. However DM's reflection module (that is by the way called
EntitiesAnalyzer) is static and runs only once at application start, so don't worry
too much about possible performance flaws. It is highly unlikely to happen.

Q: Why do you use Entity FW in DapperMagician?

A:
We strongly believe that 'reinventing the wheel' should be avoided.
These days most of applications generally should be optimized by
analyzing low-performance operations especially those that run frequently.

With today's modern storage devices it's far more important how our application
deals with complicated scenarios than how much of a physical space it consumes on the HDD.
That's why we added the whole 'EF' rather than attempted of recreating one of
its modules. Creation of database is a one-time operation so it has
no other impact, apart of increasing volume of physical space on hard disk.

Q: What the heck.. How does DapperMagician work ?

A:
Take a look at abstract class Entity and IEntity interface.
In order to let DM find all models it is absolutely imperative
that one of this duo is implemented in every model.

DM uses reflection for finding all classes in all of solution's projects that implement
IEntity or Entity<T>. After that it analyzes these types and create Map that is used
later for type mapping.

The IEntity interface has EntityIdentifier property - a system property of DM.
It could be implemented implicitly by abstract class Entity<T> where T is a
type of Id property. It's also possible to explicitly implement IEntity
from your custom class that inherits from it.


---
Thank you for using DapperMagician!

  Sincerly Yours
  CodeMagicians team