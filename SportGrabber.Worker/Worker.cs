﻿using System;
using System.Threading;
using System.Threading.Tasks;
using SportGrabber.Core.Enums;
using SportGrabber.Core.Interfaces;

namespace SportGrabber.Worker
{
    public class Worker
    {
        private readonly IParsingService _service;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        public Worker(IParsingService service)
        {
            this._service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public void Start(int pollingDelayMilliseconds)
        {
            while (!_cancellationTokenSource.Token.IsCancellationRequested)
            {
                Task.Run(() =>
                {
                    var sportTypes = Enum.GetNames(typeof(SportType));
                    Parallel.ForEach(sportTypes, new ParallelOptions
                    {
                        CancellationToken = _cancellationTokenSource.Token
                    }, sportType =>
                    {
                        _service.UpdateDatabase($"http://www.livescores.com/{sportType}");
                    });
                });
                Thread.Sleep(pollingDelayMilliseconds);
            }
        }

        public void Stop(int millisecondsDelay = 0)
        {
           _cancellationTokenSource.CancelAfter(millisecondsDelay);
        }
    }
}
