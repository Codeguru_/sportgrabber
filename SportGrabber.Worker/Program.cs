﻿using System;
using System.Threading;
using SportGrabber.Data.DapperMagician;
using SportGrabber.Worker.SimpleInjector;

namespace SportGrabber.Worker
{
    class Program
    {
        static void Main(string[] args)
        {            
            DapperMagicianConfig.Initialize();
            var container = SimpleInjectorConfig.RegisterAll();
            Console.WriteLine("Press any key to terminate process...");
            var worker = container.GetInstance<Worker>();
            worker.Start(60000);
            Console.ReadKey();
            worker.Stop();
            
            // Wait for all threads to terminate
            Thread.Sleep(3000);
        }
    }
}
