﻿using System;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace SportGrabber.Worker.SimpleInjector
{
    class SimpleInjectorConfig
    {
        public static Container RegisterAll()
        {
            var container = new Container();
            Data.SimpleInjector.Bootstrap.RegisterDependencies(container);
            Parsing.SimpleInjector.Bootstrap.RegisterDependencies(container);
            container.Register<Worker>();
            container.Verify();
            return container;
        }
    }
}