﻿using System;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using SportGrabber.Core.Interfaces;
using SportGrabber.Core.Models;
using SportGrabber.Parsing.Livescores;

namespace SportGrabber.Parcing.Tests
{
    [TestFixture]
    public class LiveScoresDocumentParserTests
    {
        private readonly Mock<IHtmlDocumentParsingStrategy<Game>> _liveScoresDocumentParsingStrategy;
        private readonly IHtmlDocumentParser<Game> _target;
        private readonly Fixture _fixture = new Fixture();

        public LiveScoresDocumentParserTests()
        {
            _liveScoresDocumentParsingStrategy = new Mock<IHtmlDocumentParsingStrategy<Game>>();
            _target = new LivescoresDocumentParser(_liveScoresDocumentParsingStrategy.Object);
        }

        [Test]
        public void Parse_CallsStrategy_UsingCorrectValues()
        {
            var url = _fixture.Create<string>();
            _target.Parse(url);
            _liveScoresDocumentParsingStrategy.Verify(x => x.Parse(url), Times.Once);
        }
    }
}
